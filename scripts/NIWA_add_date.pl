#!/usr/bin/perl

# Opens NIWA file and adds date column (taken from filename)

use warnings;
use strict;
use autodie;

use feature 'say';
use feature 'state';
my $take_each = 5;

my $niwa_files_dir = '/media/data/Study/vcsn_data';
my $output_dir = '/home/shield/Whitireia/Project/vcsn_by_years_filtered_'.$take_each;


mkdir $output_dir unless -d $output_dir;
`rm -f $output_dir/*csv`;

opendir(my $dh, $niwa_files_dir);

foreach my $filename (sort readdir($dh)) {
#     say $filename;
    
    next unless $filename =~ /(?<year>\d{4})(?<month>\d{2})(?<day>\d{2})_vcsn.dat/;
    
    my $year = $+{year};
    my $day = $+{day};
    my $month = $+{month};

    my $new_file_flag = ! -f $output_dir.'/'.$year.'.csv';

    open my $output, ">>:encoding(utf8)", $output_dir.'/'.$year.'.csv';    
    print $output "Year,Month,Day,Agent,Lat,Longt,Date,MSLP,PET,Rain,RH,SoilM,ETmp,Rad,TMax,Tmin,VP,Wind\n" if $new_file_flag;
    say $year if $new_file_flag;
        
    open my $input, "<:encoding(utf8)", $niwa_files_dir.'/'.$filename;
    while (my $line = <$input>) {
            next if $. == 0;
            next if defined $take_each && $. % $take_each;
           
            print $output "$year,$month,$day,$line";
    }
    close $input;
    close $output;

#     last unless $filename =~ /^\./; #debug stop
}

closedir $dh;




