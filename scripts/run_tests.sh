#/bin/bash

SPARK_PATH="/media/data/distr/Spark/spark"
SCRIPTS_PATH="/home/shield/Whitireia/Project/scripts"
RESULTS_PATH="/home/shield/Whitireia/Project/Results"


$SPARK_PATH/sbin/start-master.sh
echo "Check http://servoskull:8080/"
sleep 5

for CORES in {1..8}; do
    $SPARK_PATH/bin/spark-class org.apache.spark.deploy.worker.Worker --cores $CORES spark://servoskull:7077 &
    WORKER_PID=$!
    echo "WORKER_PID=$WORKER_PID"
    sleep 5
    for RUN in {1..10}; do
        echo "Cores: $CORES, run #$RUN"
        rm -rf /tmp/sparkR-tmp; 
        $SPARK_PATH/bin/sparkR --master spark://servoskull:7077 --executor-memory 14500M  $SCRIPTS_PATH/elNino.R;
    done;
    kill -15 $WORKER_PID
    rm -rf $SPARK_PATH/work/*
done;

cd $RESULTS_PATH
wget http://localhost:8080/

$SPARK_PATH/sbin/stop-master.sh
