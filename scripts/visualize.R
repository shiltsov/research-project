#!/usr/bin/env Rscript

library(ggmap)

setwd('/home/shield/Whitireia/Project/Results/')
data <- read.csv('seasons.csv', strip.white=TRUE)

map <- get_map(location = 'New Zealand', zoom = 5)

visualize <- function(Year='1973', Season='Summer', Measurement='Rain', Excolor = 'purple', Min=-5, Max=+5) {
    data_subset = data[data$Year == Year & data$Season == Season, c('Longt', 'Lat', paste0(Measurement,'_mean'), paste0(Measurement,'_diff'))]
    pdf(file = paste0(Season,'_of_',Year,'_',Measurement,'.pdf'), width = 5, height = 5)
    print(
        ggmap(map) + 
    
        geom_point(aes_string(x = 'Longt', y = 'Lat', color = paste0(Measurement,'_diff')), data = data_subset, alpha = .7, size=0.8, shape = 16) + 
    
        scale_colour_gradientn(limits=c(Min, Max), colours=rainbow(4), na.value=Excolor,name=Measurement)
    )
    dev.off()
}

visualize(Year='1998', Season='Summer', Measurement='Rain', Excolor = 'purple', Min=-6, Max=6)
visualize(Year='1983', Season='Summer', Measurement='Rain', Excolor = 'purple', Min=-6, Max=6)
visualize(Year='1976', Season='Summer', Measurement='Rain', Excolor = 'red', Min=-6, Max=6)
visualize(Year='1989', Season='Summer', Measurement='Rain', Excolor = 'purple', Min=-6, Max=6)


visualize(1998,Measurement='MSLP', Min = -1, Max=4,Excolor='purple')
visualize(1983,Measurement='MSLP', Min = -4, Max=1,Excolor='red')
visualize(1976,Measurement='MSLP', Min = -2, Max=3,Excolor='red')
visualize(1989,Measurement='MSLP', Min = -1, Max=4,Excolor='red')



