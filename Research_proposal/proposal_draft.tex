\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{apacite}
\usepackage{url}
\bibliographystyle{apacite}

\usepackage{/home/shield/Whitireia/whitireiatitle}

\title{An investigation into the efficacy of executing data mining algorithms using parallel programming techniques to improve their computational performance}
\author{Dmitry Shiltsov}
\course{IT8x21 -- Research Project}
\studentid{21402582}

\begin{document}

\begin{titlepage}

\whitireiatitle{Research project proposal}{Adrian Hargreaves}

\end{titlepage}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\tableofcontents

\section{The motivation for the research}

We live in the postindustrial society. It means that information plays the major role in economic growth. More data and more people are involved in this area with each year. One of the consequences of this popularity is a boom of specialized open source software. Traditionally, data mining tools were limited to processing data on single core machines. Nowadays, with the advent of multicore processors incorporated into even entry level laptops, parallel computing has become an affordable alternative. Unfortunately moving from the traditional single thread approach to parallel processing involves additional challenges like synchronization, load balancing, error recovery, and so on. The aim of this work is to estimate the benefits gained by this move.

\section{Research question}

What improvements in computational performance can be leveraged from applying parallel programming techniques in the execution of data mining algorithms?

\section{Literature review}

\paragraph{Data Mining}

To understand why Data Mining is important, we might look at the book of \citeA{Han_2006_Data_mining} which starts with the statement: ``we are living in the data age''. According to the book, Data Mining is the process of transformation of the raw data into usable knowledge. This can be applied not only to businesses built around the Internet, but also  to traditional industries like dairy farms. Data Mining is usually associated with Statistics, Machine Learning, Information retrieval and storage. Being an umbrella term, the application of Data Mining varies from data visualization to modeling and prediction. The article of \citeA{Fayyad_1996_From_data_mining_to_knowledge_discovery_in_databases} gives another outlook on Data Mining application.


\paragraph{R programming environment}

There are numerous statistical and Data Mining software suits available, but we will look closely at R for the number of reasons. Firstly, it is an open source product, which means it is free from vendor lock. Secondly, its popularity in both, academical and business, environments is increasing tremendously, even compared with long time leaders as Stata, Statistica, SAS, and SPSS \cite{Muenchen_2012_The_popularity_of_data_analysis_software}.  Thirdly, the language is well documented and number of books on the topic is constantly growing \cite{R:Stowell:2014} and \cite{R:Knell:2013}.  

\paragraph{Big Data}

As explained in articles of \citeA{McAfee_2012_Big_data_the_management_revolution} and \citeA{Kaisler_Big_data_Issues_and_challenges_moving_forward}, we are living in the age of Big Data where the amount of information available rises exponentially. This is a situation when all traditional solutions, relying on the use of only the researcher's computer, fail to process all the data available. For example, R alone is able to hold and process only those objects, that fit in the memory.

\paragraph {Parallel mining}

One of the most obvious answers to the Big Data problem is to use lots of computers simultaneously. This approach, which existed from the earliest days of computers, is thoroughly covered by \citeA{Abbass_2001_Data_Mining_A_Heuristic_Approach} and \citeA{Skillicorn_1999_Strategies_for_parallel_data_mining} in relation to Data Mining. Articles of \citeA{Dhillon_2000_A_data_clustering_algorithm_on_distributed_memory_multiprocessors} and \citeA{Foti_2000_Scalable_parallel_clustering_for_data_mining_on_multicomputers} demonstrate the successful application of parallel processing for different types of distributed architectures. The work of \citeA{Isard_2007_Dryad_distributed_data_parallel_programs_from_sequential_building_blocks} explores the ways to build a distributed system from multiple sequential blocks, washing away the border between linear and parallel approaches. The recent generation of research works in the area goes even further, claiming that classical tools are no longer applicable at all. One example is \cite{Zhang_2013_A_Parallel_Clustering_Algorithm_with_MPI_MKmeans}, which relies on time proven MPI libraries as a base for distributed computation. Another example is \cite{Zhanquan_2013_A_Parallel_Clustering_Method_Study_Based_on_MapReduce} with the more recent MapReduce approach.


\paragraph {Big Data frameworks}

The challenges of Big Data led to development of new tools which leverage distributed systems and parallel approach even further. In the last years Apache Hadoop, the open source implementation of Google MapReduce, became an industry standard in massive computation \cite{Dittrich_Efficient_Big_Data_Processing_in_Hadoop_MapReduce, Thusoo_Hive_a_petabyte_scale_data_warehouse_using_hadoop, Venner_Pro_Hadoop}. According to the publications of \citeA{Zaharia_2010_Spark_cluster_computing_with_working_sets} and \citeA{Zaharia_2012_Fast_and_interactive_analytics_over_Hadoop_data_with_Spark}, another open-source product from Apache foundation -- Spark, is slowly replacing Hadoop in most areas.


\paragraph {Big Data using R}

To be able to deal with the Big Data challenges, R community developed a number of solutions. The classic approach is to use MPI libraries \cite{ferreira2011rmpi}. The most apparent drawback of this approach is that it demands a lot of attention to implementation details, effectively distracting researcher from his original work \cite{yu2004rmpi}. The usage of pbdR libraries (programming with big data in R) is a step forward \cite{Ostrouchov_Programming_with_Big_Data_in_R,Raim_2013_Introduction_to_Distributed_Computing_with_pbdR_at_the_UMBC_High_Performance_Computing_Facility,Ostrouchov_2013_Combining_R_with_scalable_libraries_to_get_the_best_of_both_for_big_data,Chen_2012_A_Quick_Guide_for_the_pbdMPI_package}. Using R in the existing Big Data infrastructure is even more sensible solution. Works of \citeA{Guha_Large_Complex_Data_Divide_and_Recombine_D_R_with_RHIPE} and \citeA{Das_Ricardo_integrating_R_and_Hadoop} describe integration of R with Hadoop. Integration with Spark is presented by the R package SparkR \cite{sparkR_website,insideBIGDATA_2012_SparkR} or by publicly unavailable package RABID \cite{Lin_2014_RABID_A_Distributed_Parallel_R_for_Large_Datasets}.


\paragraph{Cloud services}

To use parallel mining, one requires not only the appropriate software, but also appropriate hardware to run it on. Many academic institutions and enterprises possess special computer grids or clusters of commodity machines to run parallel applications \cite{computing2005distributed}. Naturally, access to resources like that is limited, but with the rise of cloud services \cite{Mell_2011_The_NIST_definition_of_cloud_computing} anyone can rent a cluster for a reasonable price. While the performance of cloud machines was questionable for a while \cite{Ostermann_2010_A_performance_analysis_of_EC2_cloud_computing_services_for_scientific_computing,Iosup_2011_Performance_analysis_of_cloud_computing_services_for_many_tasks_scientific_computing}, later publications proved the benefits of this approach, especially in the area of Data Mining \cite{Sheth_2012_Implementing_Parallel_Data_Mining_Algorithm_on_High_Performance_Data_Cloud}. The question of the last two years is not 'Should we use cloud services?', but 'What services are most suitable for Big Data analysis \cite{Baru_2013_Benchmarking_big_data_systems_and_the_bigdata_top100_list}. Numerous publications address this question \cite{Baru_2012_Big_data_benchmarking,Ghazal_2013_BigBench_towards_an_industry_standard_benchmark_for_big_data_analytics,Wang_2014_Bigdatabench_a_big_data_benchmark_suite_from_internet_services,Ming_2014_BDGS_A_Scalable_Big_Data_Generator_Suite_in_big_data_benchmarking,Geoffrey_2015_Towards_a_Comprehensive_Set_of_Big_Data_Benchmarks}, but it seems, that the most universal answer is a correctly set experiment \cite{Apon_2015_Experimentation_As_a_Tool_for_the_Performance_Evaluation_of_Big_Data_Systems}.


\section{Methodology}

We will use the design science research process according to \cite{peffers2006design}:

\begin{enumerate}
\item Problem identification and motivation
\item Objectives of a solution
\item Design and development
\item Demonstration
\item Evaluation
\item Communication
\end{enumerate}

The work of \cite{von2004design} provides a detailed description of evaluation steps. The following methods will be applicable to this project:

\paragraph{Observational:} study artefact in business environment presented by Amazon EC2.
\paragraph{Analytical:} Dynamic Analysis -- evaluate the artefact performance with different number of computing nodes.
\paragraph{Experimental:} study artefact in controlled Linux environment of researcher's laptop.
\paragraph{Testing:} we will do functional (Black Box) testing without going into implementation details.
\paragraph{Descriptive:} we will construct the detailed scenario (study case) around artifact.
    
\section{Experiment description}

\subsection{Objectives of a solution}

We will pick a data mining problem and build a solution leveraging parallel computation.

\subsection{Design and development} 

\subsubsection{Software}

Based on the literature review performed we will explore the combination of R and Apache Spark \cite{sparkR_website} as the most promising solution. 

\subsubsection{Hardware} The experiment will be run in two stages:
\begin{description}
 \item[On the personal laptop] with the maximum of 8 cores.
 \item[In the Amazon EC2 cloud] with the maximum of 64 cores.
\end{description}


\subsection{Demonstration}

From numerous areas covered by Data Mining we will take data exploration and visualization as our case study. Particularly we will explore the effect of El Niño Southern Oscillation on the climate of New Zealand (Fig.~\ref{fig:elnino}). The massive dataset of observations covering years from 1972 to 2013 will provide us with enough challenges. We will analyze changes in temperature and precipitation through the time, trying to reproduce results from the works of \cite{Ummenhofer_2007_Interannual_extremes_in_New_Zealand_precipitation_linked_to_modes_of_Southern_Hemisphere_climate_variability, Ummenhofer_2009_Causes_of_late_twentieth_century_trends_in_New_Zealand_precipitation,Mosley_2000_Regional_differences_in_the_effects_of_ElNino_and_LaNina_on_low_flows_and_floods}. 

\begin{center}
    \begin{figure}[h]
    \includegraphics[width=0.9\linewidth]{d-7785-niwa.jpg}
        \caption{\label{fig:elnino} NZ precipitation anomalies (www.niwa.co.nz)}
    \end{figure}
\end{center}
 
 The fruitfulness of using Amazon EC2 for climate modeling was shown by \cite{Evangelinos_2008_atmosphere_ocean_climate_models_on_EC2}.

\subsection{Evaluation}
We will measure the scalability of the solution similarly to related publications, namely \citeA{McCreadie_2012_MapReduce_indexing_strategies_Studying_scalability_and_efficiency,Sparks_2013_MLI_An_API_for_distributed_machine_learning}. It will be especially interesting to compare results with the work of \citeA{Lin_2014_RABID_A_Distributed_Parallel_R_for_Large_Datasets} where the similar combination of underlying technologies was used (R and Apache Spark).

\subsection{Communication}
The results of the work will be published via proper report and presentation in Whitireia New Zealand.


\section{ Scope statement }

\paragraph{Aim:} measuring the scalability over the number of computational nodes.
\paragraph{Software:} only one combination of technologies (SparkR).
\paragraph{Hardware:} single machine and only one cloud provider (Amazon EC2).
\paragraph{Study case:} only one problem to solve (data exploration).


\section{Ethical considerations}
There are no ethical issues anticipated, because this work doesn't involve interaction with people or their data. We will use only publicly availible data related to climatic conditions.

\section{Intellectual property considerations} 
\paragraph{Software:} All the tools to use are open source.
\paragraph{Data:} The climate data is a property of NIWA and is publicly available.

\section{Potential contributions or outcomes of the research and their importance}
Users of R would be able to estimate potential performance gains from using Spark on local machine and in the Amazon EC2.


\section{Timeline}
\begin{center}
    \begin{tabular}{c|c|l}
        Week    & Date      & Planned \\ \hline 
        1       & 23 Feb    & Initiation \\
        2       & 2 Mar     & Work on proposal \\
        3       & 9 Mar     & Work on proposal \\
        4       & 16 Mar    & Work on proposal \\
        5       & 23 Mar    & Work on proposal \\
        6       & 30 Mar    & Getting and exploring Data, setting up Spark on a local machine  \\
        -       & 6 Apr     & - \\
        -       & 13 Apr    & - \\
        7       & 20 Apr    & Implementing the study case with SparkR \\
        8       & 27 Apr    & Performing measurements on the local machine  \\
        9       & 4 May     & Performing measurements in the cloud \\
        10      & 11 May    & Report draft \\
        11      & 18 May    & Analysing results \\
        12      & 25 May    & Work on report and poster \\
        13      & 1 Jun     & Submission of report \\
        14      & 8 Jun     & Poster submission \\
        15      & 15 Jun    & Presentation \\

    \end{tabular} 
\end{center}

\section{Resources} 

\begin{center}
    \begin{tabular}{r|l}
        \textbf{Resource} & \textbf{where to get} \\\hline
        Data & publicly available \\\hline
        R software & open source \\\hline
        Apache Spark & open source \\\hline
        Multi-core computer & provided by student \\\hline
        Access to the cloud & provided by student \\\hline
    \end{tabular} 
\end{center}



\bibliography{parallel_mining,climate,cloud,benchmarks,r,design_science}

\end{document}
