Topic
=====
An investigation into the efficacy of executing data mining algorithms using parallel programming techniques to improve their computational performance.

Research question
=================
What improvements in computational performance can be leveraged from applying parallel programming techniques in the execution of data mining algorithms?

Scope
=====
This projects involves implementing a Data Mining algorithm using R + Spark and testing its efficiency:

* comparison with a single threaded implementation;
* measuring a scalability using local machines;
* measuring a scalability using AWS.

Ideally, we might look for the article about scalability of R based solution, do the same tests with our system and compare results.

Plan
====

1. Look for the literature:
    * about SparkR;
    * about R and parallel processing;
    * about Data Mining and parallel processing;
    * about scalability measuring.
2. Choose the problem
    * Algorithm;
    * Data Source. 
3. Write a **research proposal**
4. Solve the problem
    * With usual R and a local machine;
    * With SparkR and local machines;
    * With SparkR and a real cluster.

5. Write a **report**

6. Prepare a poster 

7. Prapare a presentation